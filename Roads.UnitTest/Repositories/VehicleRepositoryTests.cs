﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Roads.Models;
using Roads.Services;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Roads.Repositories.Tests
{
    [TestClass()]
    public class VehicleRepositoryTests
    {
        IQueryable<Vehicle> vehicleSource;

        [TestInitialize()]
        public void Setup()
        {
            // Use a temporary datasource to avoid real database manipulation.
            this.vehicleSource = new List<Vehicle>() {
                new Vehicle() { Id = 1, Name = "Mazda 3", Manufacturer = "Mazda", Type = "Sedan", LicensePlate = "23-SP-ZB", Mileage = 177500, PrivateUse = true, Available = true, DateAdded = DateTime.Now.AddDays(-42) },
                new Vehicle() { Id = 2, Name = "Sub 2020", Manufacturer = "Subaru", Type = "Hatchback", LicensePlate = "44-AA-ZZ", Mileage = 12570, PrivateUse = true, Available = false, DateAdded = DateTime.Now.AddDays(-12) },
                new Vehicle() { Id = 3, Name = "Tesla S", Manufacturer = "Tesla", Type = "Sedan", LicensePlate = "1-GGA-P", Mileage = 6495, PrivateUse = false, Available = true, DateAdded = DateTime.Now },
            }.AsQueryable();
        }

        [TestMethod()]
        [Description("Test the vehicle source to return all entries.")]
        public void Get_Vehicles_All_Test()
        {
            // Arrange
            var mock = new Mock<IDatasourceProvider>();
            mock.Setup(x => x.Set<Vehicle>()).Returns(vehicleSource);

            // Act
            var context = mock.Object;
            var vehicleSet = context.Set<Vehicle>();

            // Assert
            Assert.AreEqual(vehicleSource, vehicleSet);
        }

        [TestMethod()]
        [Description("Test the vehicle source to return only available entries.")]
        public void Get_Vehicles_Available_Test()
        {
            // Arrange
            var mock = new Mock<IDatasourceProvider>();
            mock.Setup(x => x.Set<Vehicle>()).Returns(
                vehicleSource.Where(x => x.Available)
            );

            // Act
            var context = mock.Object;
            var vehicleSet = context.Set<Vehicle>();

            // Assert
            Assert.IsTrue(vehicleSet.Count() == 2, "Number of available vehicles does not match the datasource!");
            foreach(Vehicle v in vehicleSet)
            {
                Assert.IsTrue(v.Available, String.Format("Unavailable vehicles ({0}) shouldn't be present in the datasource!", v.Id));
            }
        }

        [TestMethod()]
        [Description("Test the vehicle source to return only the requested vehicle by PK.")]
        public void Get_Vehicle_By_Id_Test()
        {
            // Arrange
            int requestedVehicleId = 2;
            var mock = new Mock<IDatasourceProvider>();
            mock.Setup(x => x.Set<Vehicle>()).Returns(
                vehicleSource.Where(x => x.Id == requestedVehicleId)
            );

            // Act
            var context = mock.Object;
            var vehicleSet = context.Set<Vehicle>();

            // Assert
            Assert.IsTrue(vehicleSet.Count() == 1, "Number of vehicles should be only one when requesting by ID!");
            if (vehicleSet.Any())
            {
                Assert.AreEqual(vehicleSet.First().Id, requestedVehicleId, "The returned vehicle ID does not match the requested ID!");
            }
        }

        [TestMethod()]
        [Description("Test the vehicle source to add a new entry.")]
        public void Add_Vehicle_Test()
        {
            // Arrange
            Vehicle vehicle = new Vehicle() {
                Id = 4,
                Name = "Supra X",
                Manufacturer = "Toyota",
                Type = "Sedan",
                LicensePlate = "SU-PRA-2",
                Mileage = 251000,
                PrivateUse = false,
                Available = true,
                DateAdded = DateTime.Now
            };
            var mock = new Mock<IDatasourceProvider>();
            mock.Setup(x => x.Set<Vehicle>()).Returns(vehicleSource);

            // Act
            var contextBefore = mock.Object;
            var vehicleSetBefore = contextBefore.Set<Vehicle>();

            // Add the entry to the vehicle source and retrieve the updated dataset.
            this.vehicleSource = this.vehicleSource.Concat(new Vehicle[] { vehicle });
            mock.Setup(x => x.Set<Vehicle>()).Returns(vehicleSource);
            var contextAfter = mock.Object;
            var vehicleSetAfter = contextAfter.Set<Vehicle>();

            // Assert
            Assert.AreNotEqual(vehicleSetBefore.Count(), vehicleSetAfter.Count(), "The amount of vehicles in the vehicle source should not stay the same!");
            Assert.IsTrue(vehicleSetBefore.Count() + 1 == vehicleSetAfter.Count(), "The amount of vehicles in the vehicle source should increment by 1!");
            // Validate the existance of the new entry.
            Vehicle newVehicle = vehicleSetAfter.Where(x => x.Id == vehicle.Id).FirstOrDefault();
            Assert.AreEqual(vehicle, newVehicle, "The added vehicle should match the supplied instance!");
        }

        [TestMethod()]
        [Description("Test the vehicle source to update an existing entry.")]
        public void Update_Vehicle_Test()
        {
            // Arrange
            int requestedVehicleId = 3;
            int mileageBefore = 6495;
            var mock = new Mock<IDatasourceProvider>();
            mock.Setup(x => x.Set<Vehicle>()).Returns(vehicleSource);

            // Act
            var contextBefore = mock.Object;
            var vehicleSetBefore = contextBefore.Set<Vehicle>();

            // Update the entry of the vehicle source and retrieve the updated dataset.
            List<Vehicle> tempVehicleSource = this.vehicleSource.ToList();
            tempVehicleSource[requestedVehicleId - 1].Mileage += 257;
            this.vehicleSource = tempVehicleSource.AsQueryable();
            mock.Setup(x => x.Set<Vehicle>()).Returns(vehicleSource);
            var contextAfter = mock.Object;
            var vehicleSetAfter = contextAfter.Set<Vehicle>();

            // Assert
            Assert.AreEqual(vehicleSetBefore.Count(), vehicleSetAfter.Count(), "The amount of vehicles in the vehicle source should stay the same!");
            // Validate the changes of the updated entry.
            Vehicle updatedVehicle = vehicleSetAfter.Where(x => x.Id == requestedVehicleId).FirstOrDefault();
            Assert.AreEqual(mileageBefore + 257, updatedVehicle.Mileage, "The updated vehicle should have a mileage increased by 257!");
        }

        [TestMethod()]
        [Description("Test the vehicle source to remove the given entry.")]
        public void Delete_Vehicle_Test()
        {
            // Arrange
            int requestedVehicleId = 2;
            var mock = new Mock<IDatasourceProvider>();
            mock.Setup(x => x.Set<Vehicle>()).Returns(vehicleSource);

            // Act
            var contextBefore = mock.Object;
            var vehicleSetBefore = contextBefore.Set<Vehicle>();

            // Exclude the entry from the vehicle source and retrieve the updated dataset.
            this.vehicleSource = this.vehicleSource.Where(x => x.Id != requestedVehicleId);
            mock.Setup(x => x.Set<Vehicle>()).Returns(vehicleSource);
            var contextAfter = mock.Object;
            var vehicleSetAfter = contextAfter.Set<Vehicle>();

            // Assert
            Assert.AreNotEqual(vehicleSetBefore.Count(), vehicleSetAfter.Count(), "The amount of vehicles in the vehicle source should not stay the same!");
            Assert.IsTrue(vehicleSetBefore.Count() - 1 == vehicleSetAfter.Count(), "The amount of vehicles in the vehicle source should decremented by 1!");
            // Make sure the 'removed' vehicle doesn't exist anymore.
            Vehicle unknownVehicle = vehicleSetAfter.Where(x => x.Id == requestedVehicleId).FirstOrDefault();
            Assert.IsNull(unknownVehicle, "The removed vehicle should not return an existing instance!");
        }
    }
}