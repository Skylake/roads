﻿using System.ComponentModel;
using System.Collections.Generic;
using System.Linq;
using MapKit;
using Roads.Controls;
using Roads.iOS.Renderers;
using Xamarin.Forms;
using Xamarin.Forms.Maps.iOS;
using Xamarin.Forms.Platform.iOS;
using Xamarin.Forms.Maps;
using System.Collections.Specialized;

[assembly:ExportRenderer(typeof(CustomMap),
    typeof(CustomMapRenderer))]
namespace Roads.iOS.Renderers
{
    public class CustomMapRenderer : MapRenderer
    {
        MKMapView mapView;
        CustomMap customMap;
        Polyline polyline = new Polyline
        {
            StrokeColor = Color.FromHex("#0081d1"),
            StrokeWidth = 5,
        };

        public bool pointsLoaded { get; set; } = false;
        private void Points_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            System.Diagnostics.Debug.WriteLine("--- Map collection changed, Points found: {0}, Before: {1}, After: {2}! ---",
                customMap.Points.Count, e.OldItems?.Count, e.NewItems?.Count);

            if (e.NewItems?.Count != null) {
                LoadPolyPointers(e.NewItems.Cast<Models.Point>());
            }
        }

        protected override void OnElementPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            base.OnElementPropertyChanged(sender, e);

            // Assign map objects if unset and start tracking changes in Points.
            if (this.mapView is null)
                this.mapView = (MKMapView)Control;

            if (this.customMap is null)
            {
                this.customMap = (CustomMap)Element;
            }

            // Wait till the required data is loaded.
            if (!pointsLoaded && this.customMap.Points.Count > 2)
            {
                try
                {
                    // Load coordinates and set markers.
                    LoadPolyPointers(this.customMap.Points);
                    LoadMarkerPointers(this.customMap.Points);

                    // Add the 'CollectionChanged' event listener.
                    this.customMap.Points.CollectionChanged += Points_CollectionChanged;

                    // Mark 'pointers' as loaded.
                    pointsLoaded = true;
                }
                catch
                {
                    // Failed to load, wait for another try.
                    pointsLoaded = false;
                }
            }
        }

        // Function that loads data from the given points.
        private void LoadPolyPointers(IEnumerable<Models.Point> points)
        {
            System.Diagnostics.Debug.WriteLine("LoadPolyPointers -> Found {0} points!", points.Count());
            if (!points.Any())
                return;

            foreach (Models.Point point in points)
            {
                System.Diagnostics.Debug.WriteLine("New added item: {0}, {1}, {2}, {3}.",
                    point.Location.Id, point.Location.TripId, point.Location.Latitude, point.Location.Longitude);

                polyline.Geopath.Add(new Position(point.Location.Latitude, point.Location.Longitude));
            }
            customMap.MapElements.Add(polyline);
        }

        // Function that adds pin markers to the map using the given position.
        private void LoadMarkerPointers(IEnumerable<Models.Point> points)
        {
            if (!points.Any())
                return;

            // Add a start position marker.
            customMap.Pins.Add(new Pin
            {
                Label = "Route Started",
                Address = "First registered position.",
                Type = PinType.SavedPin,
                Position = new Position(
                    points.First().Location.Latitude,
                    points.First().Location.Longitude
                )
            });

            // Add a stop position marker.
            customMap.Pins.Add(new Pin
            {
                Label = "Route Checkpoint",
                Address = "Last registered position.",
                Type = PinType.SavedPin,
                Position = new Position(
                    points.Last().Location.Latitude,
                    points.Last().Location.Longitude
                )
            });
        }

        protected override void Dispose(bool disposing)
        {
            if (this.customMap?.Points != null)
                this.customMap.Points.CollectionChanged -= Points_CollectionChanged;

            base.Dispose(disposing);
        }
    }
}