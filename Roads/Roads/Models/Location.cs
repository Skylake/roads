﻿using System;
using SQLite;

namespace Roads.Models
{
    public class Location // Rename to Position?
    {
        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }
        public int TripId { get; set; }
        public double Latitude { get; set; }
        public double Longitude { get; set; }
        public DateTime Timestamp { get; set; }
        public double Speed { get; set; }

        public Location() { }
        public Location(int TripId, double Latitude, double Longitude, DateTime Timestamp, double Speed)
        {
            this.TripId = TripId;
            this.Latitude = Latitude;
            this.Longitude = Longitude;
            this.Timestamp = Timestamp;
            this.Speed = Speed;
        }
    }
}
