﻿using System;
using SQLite;

namespace Roads.Models
{
    public class Trip
    {
        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }
        [Indexed]
        public int VehicleId { get; set; }
        [Unique]
        public string Name { get; set; }
        public string Driver { get; set; }
        public string OrderNumber { get; set; }
        public DateTime? DateTripStart { get; set; }
        public DateTime? DateTripFinish { get; set; }
        public string LocationStartAddress { get; set; }
        public string LocationFinishAddress { get; set; }
        public int LocationStartId { get; set; } // First Location.id
        public int LocationFinishId { get; set; } // Final Location.id
        public int MileageStart { get; set; }
        public int MileageFinish { get; set; }
        public int Duration { get; set; }
        public double Distance { get; set; }
        public double Speed { get; set; }
        public bool Finished { get; set; }

        // TODO: Add driver.
    }
}