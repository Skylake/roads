﻿using System;
using System.Collections.Generic;
using SQLite;

namespace Roads.Models
{
    public class Vehicle
    {
        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }
        [Unique]
        public string Name { get; set; }
        public string Manufacturer { get; set; }
        public string Type { get; set; }
        public string LicensePlate { get; set; }
        public int Mileage { get; set; }
        public bool PrivateUse { get; set; }
        public bool Available { get; set; }
        public DateTime DateAdded { get; set; }

        // Keep a list of trips attached to the given vehicle.
        [Ignore]
        public List<Trip> Trips { get; set; }

        public Vehicle()
        {
            Trips = new List<Trip>();
        }
    }
}
