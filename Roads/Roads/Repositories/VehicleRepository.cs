﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using Roads.Models;
using Roads.Services;
using SQLite;

namespace Roads.Repositories
{
    public class VehicleRepository : IVehicleRepository
    {
        private SQLiteAsyncConnection connection;
        public VehicleRepository(DatasourceProvider datasourceProvider)
        {
            this.connection = datasourceProvider.GetDatasourceConnection();
        }

        public event EventHandler<Vehicle> OnItemAdded;
        public event EventHandler<Vehicle> OnItemUpdated;
        public event EventHandler<Vehicle> OnItemDeleted;

        public async Task<List<Vehicle>> GetItems()
        {
            return await connection.Table<Vehicle>().ToListAsync();
        }

        public async Task<List<Vehicle>> GetAvailableItems()
        {
            return await connection.Table<Vehicle>()
                .Where(x => x.Available)
                .ToListAsync();
        }

        public async Task<Vehicle> GetItem(int vehicleId)
        {
            return await connection.Table<Vehicle>()
                .Where(x => x.Id == vehicleId)
                .FirstOrDefaultAsync();
        }

        public async Task AddItem(Vehicle vehicle)
        {
            await connection.InsertAsync(vehicle);
            OnItemAdded?.Invoke(this, vehicle);
        }

        public async Task UpdateItem(Vehicle vehicle)
        {
            await connection.UpdateAsync(vehicle);
            OnItemUpdated?.Invoke(this, vehicle);
        }

        public async Task AddOrUpdate(Vehicle vehicle)
        {
            if (vehicle.Id == 0)
            {
                await AddItem(vehicle);
            }
            else
            {
                await UpdateItem(vehicle);
            }
        }

        public async Task DeleteItem(Vehicle vehicle)
        {
            // Eventually check if item exists using FindAsync.
            await connection.DeleteAsync(vehicle);
            OnItemDeleted?.Invoke(this, vehicle);
        }
    }
}
