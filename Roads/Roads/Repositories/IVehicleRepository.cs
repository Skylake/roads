﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Roads.Models;

namespace Roads.Repositories
{
    public interface IVehicleRepository
    {
        event EventHandler<Vehicle> OnItemAdded;
        event EventHandler<Vehicle> OnItemUpdated;
        event EventHandler<Vehicle> OnItemDeleted;

        Task<List<Vehicle>> GetItems();
        Task<List<Vehicle>> GetAvailableItems();
        Task <Vehicle> GetItem(int vehicleId);
        Task AddItem(Vehicle vehicle);
        Task UpdateItem(Vehicle vehicle);
        Task AddOrUpdate(Vehicle vehicle);
        Task DeleteItem(Vehicle vehicle);
    }
}
