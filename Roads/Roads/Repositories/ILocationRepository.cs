﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Threading.Tasks;
using Roads.Models;


namespace Roads.Repositories
{
    public interface ILocationRepository
    {
        event EventHandler<Location> OnItemAdded;

        Task Save(Location location);
        Task<List<Location>> GetAll();
        Task<List<Location>> GetTripRoute(Trip trip);
        Task DeleteTripRoute(Trip trip);
        Task DeleteAll();
    }
}
