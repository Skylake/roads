﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Roads.Models;

namespace Roads.Repositories
{
    public interface ITripRepository
    {
        event EventHandler<Trip> OnItemAdded;
        event EventHandler<Trip> OnItemUpdated;
        event EventHandler<Trip> OnItemDeleted;

        Task<List<Trip>> GetItems();

        Task<List<Trip>> GetVehicleTrips(int vehicleId);
        Task<Location> GetTripLastPosition(int tripId);

        Task AddItem(Trip trip);
        Task UpdateItem(Trip trip);
        Task AddOrUpdate(Trip trip);
        Task DeleteItem(Trip trip);
    }
}