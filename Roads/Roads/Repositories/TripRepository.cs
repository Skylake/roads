﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using Roads.Models;
using Roads.Services;
using SQLite;

namespace Roads.Repositories
{
    public class TripRepository : ITripRepository
    {
        private SQLiteAsyncConnection connection;

        public TripRepository(DatasourceProvider datasourceProvider)
        {
            this.connection = datasourceProvider.GetDatasourceConnection();
        }

        public event EventHandler<Trip> OnItemAdded;
        public event EventHandler<Trip> OnItemUpdated;
        public event EventHandler<Trip> OnItemDeleted;

        public async Task<List<Trip>> GetItems()
        {
            return await connection.Table<Trip>().ToListAsync();
        }

        public async Task<List<Trip>> GetVehicleTrips(int vehicleId)
        {
            return await connection.Table<Trip>()
                .Where(x => x.VehicleId == vehicleId).ToListAsync();
        }

        public async Task<Location> GetTripLastPosition(int tripId)
        {
            return await connection.Table<Location>()
                .Where(x => x.TripId == tripId)
                .OrderByDescending(x => x.Id).FirstAsync();
        }

        public async Task AddItem(Trip trip)
        {
            await connection.InsertAsync(trip);
            OnItemAdded?.Invoke(this, trip);
        }

        public async Task UpdateItem(Trip trip)
        {
            await connection.UpdateAsync(trip);
            OnItemUpdated?.Invoke(this, trip);
        }

        public async Task AddOrUpdate(Trip trip)
        {
            if (trip.Id == 0)
            {
                await AddItem(trip);
            }
            else
            {
                await UpdateItem(trip);
            }
        }

        public async Task DeleteItem(Trip trip)
        {
            // Eventually check if item exists using FindAsync.
            await connection.DeleteAsync(trip);
            OnItemDeleted?.Invoke(this, trip);
        }
    }
}
