﻿using Autofac;
using Roads.Repositories;
using Roads.Services;
using Roads.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using Xamarin.Forms;

namespace Roads
{
    public class Bootstrapper
    {
        protected ContainerBuilder ContainerBuilder { get; private set; }

        public Bootstrapper()
        {
            Initialize();
            FinishInitialization();
        }

        protected virtual void Initialize()
        {
            ContainerBuilder = new ContainerBuilder();
            var currentAssembly = Assembly.GetExecutingAssembly();
            
            foreach (var type in currentAssembly.DefinedTypes
                .Where(e =>
                       e.IsSubclassOf(typeof(Page)) ||
                       e.IsSubclassOf(typeof(ViewModel))))
            {
                ContainerBuilder.RegisterType(type.AsType());
            }

            ContainerBuilder.RegisterType<DatasourceProvider>().SingleInstance();

            ContainerBuilder.RegisterType<VehicleRepository>().SingleInstance();
            ContainerBuilder.RegisterType<TripRepository>().SingleInstance();

            ContainerBuilder.RegisterType<LocationRepository>()
                .As<ILocationRepository>().SingleInstance();

            ContainerBuilder.RegisterType<LocationTrackingService>()
                .As<ILocationTrackingService>().SingleInstance();
        }

        private void FinishInitialization()
        {
            var container = ContainerBuilder.Build();
            Resolver.Initialize(container);
        }
    }
}
