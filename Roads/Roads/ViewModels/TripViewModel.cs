﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;
using Plugin.Geolocator;
using Roads.Models;
using Roads.Repositories;
using Roads.Services;
using Roads.Views;
using Xamarin.Essentials;
using Xamarin.Forms;

namespace Roads.ViewModels
{
    public class TripViewModel : ViewModel
    {
        private TripRepository tripRepository;
        private VehicleRepository vehicleRepository;

        // View conditions.
        public bool createNewTrip { get; set; }
        public bool existingUnstartedTrip { get; set; }
        public bool existingUnfishedTrip { get; set; }

        // Store the selected trip.
        private Trip trip;
        public Trip Trip
        {
            get { return trip; }
            set
            {
                if (trip != value)
                {
                    // Decide what actions should be available.
                    createNewTrip = (value.Id == 0);
                    existingUnstartedTrip = (!createNewTrip && value.LocationStartId == 0);
                    existingUnfishedTrip = (!createNewTrip && !value.Finished);

                    Debug.WriteLine("createNewTrip = {0}, existingUnstartedTrip = {1}, existingUnfishedTrip = {2}",
                        createNewTrip, existingUnstartedTrip, existingUnfishedTrip);
                    // RaisePropertyChanged(nameof(createNewTrip), nameof(existingUnstartedTrip), nameof(existingUnfishedTrip));


                    // Update trip value and notify the vm about the change.
                    trip = value;
                    RaisePropertyChanged(nameof(Trip));

                    // Load the selected vehicle of the existing trip.
                    if (value.Id != 0)
                    {
                        SelectedVehicle = Vehicles.FirstOrDefault(v => v.Id == Trip.VehicleId) ?? Vehicles.FirstOrDefault();
                        Debug.WriteLine("App.VehicleId = " + Trip.VehicleId);
                        Debug.WriteLine("SelectedVehicle.Id = " + selectedVehicle.Id);
                    }
                }
            }
        }

        // To display a list of available vehicles.
        private ObservableCollection<Vehicle> vehicles;
        public ObservableCollection<Vehicle> Vehicles
        {
            get { return vehicles; }
            set
            {
                if (vehicles != value)
                {
                    vehicles = value;
                    RaisePropertyChanged(nameof(Vehicles));
                }
            }
        }

        // Keep and notify changes of the selected vehicle and update trip accordingly.
        private Vehicle selectedVehicle;
        public Vehicle SelectedVehicle
        {
            get { return selectedVehicle; }
            set
            {
                if (selectedVehicle != value)
                {
                    selectedVehicle = value;
                    Trip.VehicleId = value.Id;
                    RaisePropertyChanged(nameof(SelectedVehicle), nameof(Trip));
                }
            }
        }

        public TripViewModel(TripRepository tripRepository,
                  VehicleRepository vehicleRepository)
        {
            this.tripRepository = tripRepository;
            this.vehicleRepository = vehicleRepository;

            // Initialize Trip to create new ones.
            this.Trip = new Trip();
            this.createNewTrip = true;
            this.existingUnstartedTrip = false;
            this.existingUnfishedTrip = false;

            Task.Run(async () => await LoadData());
        }

        private async Task LoadData()
        {
            Vehicles = new ObservableCollection<Vehicle>
                (await vehicleRepository.GetAvailableItems());
        }

        public ICommand Save => new Command(async () =>
        {
            // Some validation, a vehicle selection and name is required.
            if (String.IsNullOrWhiteSpace(Trip.Name) || Trip.VehicleId.Equals(0))
            {
                await App.Current.MainPage.DisplayAlert("Trip incomplete",
                    "One or more required fields are missing.", "OK");
                return;
            }

            // Save the trip to the repository and navigate back to overview.
            await tripRepository.AddOrUpdate(Trip);
            await Navigation.PopAsync();
        });

        public ICommand Delete => new Command(async () =>
        {
            // Only try to remove if PK is assigned.
            if (Trip.Id != 0)
            {
                await tripRepository.DeleteItem(Trip);
            }

            await Navigation.PopAsync();
        });

        public ICommand TripManage => new Command(async () =>
        {
            var routeView = Resolver.Resolve<RouteView>();
            var vm = routeView.BindingContext as RouteViewModel;
            vm.Trip = Trip;
            vm.Vehicle = SelectedVehicle;
            // Show the kept travel statistics.
            vm.travelDuration = TimeSpan.FromSeconds(Trip.Duration);
            vm.travelDistance = Trip.Distance;
            vm.travelSpeedAvg = Trip.Speed;
            await Navigation.PushAsync(routeView);
        });

        public ICommand OpenMap => new Command(async () =>
        {
            var mapView = Resolver.Resolve<MapView>();
            var vm = mapView.BindingContext as MapViewModel;
            vm.Trip = Trip;
            vm.lockedTracking = false;
            await Navigation.PushAsync(mapView);
        });
    }
}