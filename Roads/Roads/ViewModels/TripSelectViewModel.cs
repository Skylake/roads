﻿using System.Threading.Tasks;
using System.Windows.Input;
using Roads.Repositories;
using Xamarin.Forms;
using Roads.Views;
using System.Collections.ObjectModel;
using System.Linq;
using Roads.Models;
using System;
using Roads.Services;
using Xamarin.Essentials;
using System.Diagnostics;

namespace Roads.ViewModels
{
    public class TripSelectViewModel : ViewModel
    {
        private readonly TripRepository tripRepository;
        private readonly VehicleRepository vehicleRepository;

        public ObservableCollection<TripItemViewModel> Trips { get; set; }

        public TripSelectViewModel(TripRepository tripRepository,
                                   VehicleRepository vehicleRepository)
        {
            // todo: something is crashing here on trip save?
            tripRepository.OnItemAdded   += (sender, trip) => Trips.Add(CreateTripItemViewModel(trip));
            tripRepository.OnItemUpdated += (sender, trip) => Task.Run(async () => await LoadData());
            tripRepository.OnItemDeleted += (sender, trip) => Task.Run(async () => await LoadData());  // Trips.Where(v => v.Trip.Id != trip.Id);

            this.tripRepository = tripRepository;
            this.vehicleRepository = vehicleRepository;

            // Load the data used in this model.
            Task.Run(async () => await LoadData());
        }

        private async Task LoadData()
        {
            // Get a list of trip items to display.
            var trips = await tripRepository.GetItems();

            // Eventually filter out finished trips.
            if (!ShowAll)
                trips = trips.Where(x => x.Finished == false).ToList();

            // Generate trip view models and set them to Trips.
            var tripViewModels = trips.Select(i => CreateTripItemViewModel(i));
            Trips = new ObservableCollection<TripItemViewModel>(tripViewModels);
        }

        private TripItemViewModel CreateTripItemViewModel(Trip trip)
        {
            var tripViewModel = new TripItemViewModel(trip);
            tripViewModel.ItemStatusChanged += ItemStatusChanged;
            return tripViewModel;
        }

        private void ItemStatusChanged(object sender, EventArgs e)
        {
            if (sender is TripItemViewModel trip)
            {
                if (!ShowAll && trip.Trip.Finished)
                {
                    Trips.Remove(trip);
                }

                Task.Run(async () => await tripRepository.UpdateItem(trip.Trip));
            }

        }
        public bool ShowAll { get; set; }

        public ICommand ManageVehicles => new Command(async () =>
        {
            var selectVehicleView = Resolver.Resolve<VehicleSelectView>();
            await Navigation.PushAsync(selectVehicleView);
        });

        public ICommand AddItem => new Command(async () =>
        {
            var itemView = Resolver.Resolve<TripDetailView>();
            await Navigation.PushAsync(itemView);
        });

        public TripItemViewModel SelectedItem
        {
            get { return null; }
            set
            {
                Device.BeginInvokeOnMainThread(async () => await NavigateToItem(value));
                RaisePropertyChanged(nameof(SelectedItem));
            }
        }

        private async Task NavigateToItem(TripItemViewModel trip)
        {
            if (trip == null)
            {
                return;
            }

            var itemView = Resolver.Resolve<TripDetailView>();
            var vm = itemView.BindingContext as TripViewModel;
            vm.Trip = trip.Trip;
            await Navigation.PushAsync(itemView);
        }

        public string FilterText => ShowAll ? "All" : "Unfinished";

        public ICommand ToggleFilter => new Command(async () =>
        {
            ShowAll = !ShowAll;
            await LoadData();
        });
    }
}


