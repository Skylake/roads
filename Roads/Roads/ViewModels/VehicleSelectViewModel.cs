﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;
using Roads.Models;
using Roads.Repositories;
using Roads.Views;
using Xamarin.Forms;

namespace Roads.ViewModels
{
    public class VehicleSelectViewModel : ViewModel
    {
        private readonly VehicleRepository repository;

        public ObservableCollection<VehicleItemViewModel> Vehicles { get; set; }

        public VehicleSelectViewModel(VehicleRepository repository)
        {
            repository.OnItemAdded += (sender, vehicle) => Vehicles.Add(CreateVehicleItemViewModel(vehicle));
            repository.OnItemUpdated += (sender, vehicle) => Task.Run(async () => await LoadData());
            repository.OnItemDeleted += (sender, vehicle) => Task.Run(async () => await LoadData());  // Vehicles.Where(v => v.Vehicle.Id != vehicle.Id);

            this.repository = repository;
            Task.Run(async () => await LoadData());
        }

        private async Task LoadData()
        {
            var vehicles = await repository.GetItems();

            if (!ShowAll)
            {
                vehicles = vehicles.Where(x => x.Available == true).ToList();
            }

            var vehicleViewModels = vehicles.Select(i => CreateVehicleItemViewModel(i));
            Vehicles = new ObservableCollection<VehicleItemViewModel>(vehicleViewModels);
        }

        private VehicleItemViewModel CreateVehicleItemViewModel(Vehicle vehicle)
        {
            var vehicleViewModel = new VehicleItemViewModel(vehicle);
            vehicleViewModel.ItemStatusChanged += ItemStatusChanged;
            return vehicleViewModel;
        }

        private void ItemStatusChanged(object sender, EventArgs e)
        {
            if (sender is VehicleItemViewModel vehicle)
            {
                if (!ShowAll && vehicle.Vehicle.Available)
                {
                    Vehicles.Remove(vehicle);
                }

                Task.Run(async () => await repository.UpdateItem(vehicle.Vehicle));
            }

        }
        public bool ShowAll { get; set; }

        public ICommand AddItem => new Command(async () =>
        {
            var itemView = Resolver.Resolve<VehicleDetailView>();
            await Navigation.PushAsync(itemView);
        });

        //public ICommand OpenMap => new Command(async () =>
        //{
        //    var mapView = Resolver.Resolve<MapView>();
        //    await Navigation.PushAsync(mapView);
        //});

        public VehicleItemViewModel SelectedItem
        {
            get { return null; }
            set
            {
                Device.BeginInvokeOnMainThread(async () => await NavigateToItem(value));
                RaisePropertyChanged(nameof(SelectedItem));
            }
        }

        private async Task NavigateToItem(VehicleItemViewModel vehicle)
        {
            if (vehicle == null)
            {
                return;
            }

            var itemView = Resolver.Resolve<VehicleDetailView>();
            var vm = itemView.BindingContext as VehicleViewModel;
            vm.Vehicle = vehicle.Vehicle;

            await Navigation.PushAsync(itemView);
        }

        public string FilterText => ShowAll ? "All" : "Available";

        public ICommand ToggleFilter => new Command(async () =>
        {
            ShowAll = !ShowAll;
            await LoadData();
        });
    }
}