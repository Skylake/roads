﻿using System;
using System.Windows.Input;
using Roads.Models;
using Roads.Repositories;
using Xamarin.Forms;

namespace Roads.ViewModels
{
    public class VehicleItemViewModel : ViewModel
    {
        public VehicleItemViewModel(Vehicle vehicle) => Vehicle = vehicle;

        public event EventHandler ItemStatusChanged;
        public Vehicle Vehicle { get; private set; }
        public string StatusText => Vehicle.Available ? "Available" : "Unavailable";
        public string InformationText => string.Format("Geregistreerd op {0}, Kenteken {1}.",
            Vehicle.DateAdded.ToString("MMMM d, yyyy"), Vehicle.LicensePlate);

        public ICommand ToggleAvailable => new Command((arg) =>
        {
            Vehicle.Available = !Vehicle.Available;
            ItemStatusChanged?.Invoke(this, new EventArgs());
        });
    }
}
