﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Diagnostics;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Input;
using Plugin.Geolocator;
using Plugin.Geolocator.Abstractions;
using Roads.Models;
using Roads.Repositories;
using Roads.Services;
using Roads.Views;
using Xamarin.Essentials;
using Xamarin.Forms;

namespace Roads.ViewModels
{
    public class RouteViewModel : ViewModel
    {
        private readonly TripRepository tripRepository;
        private readonly VehicleRepository vehicleRepository;
        private readonly ILocationRepository locationRepository;
        private readonly ILocationTrackingService locationTrackingService;
        // private IGeolocator ICrossLocatorService { get; set; }

        public Trip Trip { get; set; }
        public Vehicle Vehicle { get; set; }
        public Models.Location LocationNext { get; set; }
        public ObservableCollection<Models.Location> Positions { get; set; }

        public bool trackingEnabled { get; set; }
        public RouteViewModel(TripRepository tripRepository,
            VehicleRepository vehicleRepository,
            ILocationRepository locationRepository,
            ILocationTrackingService locationTrackingService)
        {
            this.tripRepository = tripRepository;
            this.vehicleRepository = vehicleRepository;
            this.locationRepository = locationRepository;
            this.locationTrackingService = locationTrackingService;

            this.Trip = new Trip();
            this.Vehicle = new Vehicle();
            this.Positions = new ObservableCollection<Models.Location>();
            this.LocationNext = new Models.Location();
            // ^ Get positions here? (await vehicleRepository.GetItems());
            this.trackingEnabled = false;
        }

        private CancellationTokenSource cts;
        public void StartUpdate()
        {
            if (cts != null) cts.Cancel();
            cts = new CancellationTokenSource();
            var ignore = UpdaterAsync(cts.Token);
        }

        public void StopUpdate()
        {
            if (cts != null) cts.Cancel();
            cts = null;
        }

        public async Task UpdaterAsync(CancellationToken ct)
        {
            while (!ct.IsCancellationRequested)
            {
                // Set the new total travel duration.
                travelDuration = DateTime.Now - Trip.DateTripStart.Value;

                // delay is in ms, update every second.
                await Task.Delay(1000, ct);
            }
        }

        public ICommand TripStartService => new Command(async () =>
        {
            // Delete existing trip information.
            await this.locationRepository.DeleteTripRoute(Trip);

            // Remove existing data.
            Positions.Clear();

            // Update trip information with start position details.
            Trip.DateTripStart = DateTime.Now;
            Trip.MileageStart = Vehicle.Mileage;
            // Driven distance will be added to MileageFinish
            // (keeps progress by subtracting MileageStart).
            Trip.MileageFinish = Vehicle.Mileage;
            Trip.Duration = 0;
            Trip.Distance = 0;
            Trip.Speed = 0;

            // Start the location tracking service.
            InvokeTripServiceListener();
        });

        public ICommand TripResumeService => new Command(() =>
        {
            // This call does NOT remove previous data.
            // Instead, new driven distance is added up.

            // Start the location tracking service.
            InvokeTripServiceListener();
        });

        public TimeSpan travelDuration { get; set; }
        public double travelDistance { get; set; }
        public double travelSpeedAvg { get; set; }
        private Models.Location LocationPrev { get; set; }
        private async void InvokeTripServiceListener()
        {
            // Reset averages.
            travelDuration = new TimeSpan();
            travelDistance = 0.0;
            travelSpeedAvg = 0.0;
            LocationPrev = new Models.Location();

            // First make sure the permissions are available (required for Android, iOS uses info.plist).
            var status = await Permissions.CheckStatusAsync<Permissions.LocationAlways>();
            if (!status.HasFlag(PermissionStatus.Granted))
            {
                status = await Permissions.RequestAsync<Permissions.LocationAlways>();
                if (!status.HasFlag(PermissionStatus.Granted))
                {
                    await App.Current.MainPage.DisplayAlert("Route permissions",
                        "Location permissions are required to track routes, please try again and accept the permission request.", "OK");
                    return;
                }
            }

            // Remove navigating back until the user stopped tracking.
            NavigationPage.SetHasBackButton(Navigation.NavigationStack[Navigation.NavigationStack.Count - 1], false);
            this.trackingEnabled = true;

            Debug.WriteLine("Starting trip service listener for ID: {0}.", Trip.Id);
            await locationTrackingService.StartServiceListening();

            // TODO: Eventually pass this with locationTrackingService?
            CrossGeolocator.Current.PositionChanged += PositionChanged;

            // Update stats.
            StartUpdate();
        }

        public ICommand TripStop => new Command(async () =>
        {
            Debug.WriteLine("Stopping trip listener for ID: {0}.", Trip.Id);

            // TODO: Eventually pass this with locationTrackingService?
            CrossGeolocator.Current.PositionChanged -= PositionChanged;

            await locationTrackingService.StopListening();

            // Stop updating stats.
            StopUpdate();

            // Enable navigating back since the user stopped tracking.
            NavigationPage.SetHasBackButton(Navigation.NavigationStack[Navigation.NavigationStack.Count - 1], true);
            this.trackingEnabled = false;

            // Assign positions to access them and get first and last position.
            //this.Positions = locationTrackingService.GetPositions();
            if (this.Positions.Count <= 1)
            {
                Debug.WriteLine("Unable to get start and finish position if no positions are present.");
                await App.Current.MainPage.DisplayAlert("Route canceled",
                        "Route was to short to finish, the tracking was canceled instead.", "OK");
                return;
            }

            Models.Location PositionStart  = this.Positions[0];
            Models.Location PositionFinish = this.Positions[this.Positions.Count - 1];

            string PositionStartAddress = await locationTrackingService.GetPositionAddress(PositionStart);
            string PositionFinishAddress = await locationTrackingService.GetPositionAddress(PositionFinish);

            // Update trip information with start position details.
            Trip.LocationStartId = PositionStart.Id;
            Trip.LocationStartAddress = PositionStartAddress;

            // Update trip information with finish position details.
            Trip.DateTripFinish = DateTime.Now;
            Trip.LocationFinishId = PositionFinish.Id;
            Trip.LocationFinishAddress = PositionFinishAddress;

            // Add the new driven distance to the 'finish' mileage.
            Trip.MileageFinish += (int)Math.Round(travelDistance);

            // Update statistics.
            Trip.Duration = (int)Math.Round(travelDuration.TotalSeconds);
            Trip.Distance = travelDistance;
            Trip.Speed = travelSpeedAvg;

            await tripRepository.AddOrUpdate(Trip);

            // HACK: Manipulate NavigationStack items till updatable stack solution is found.
            // Eventually update 'navigation backwards' page instead.
            // Remove previous two navigation stacks.
            Navigation.RemovePage(Navigation.NavigationStack[Navigation.NavigationStack.Count - 1]); // Route View
            Navigation.RemovePage(Navigation.NavigationStack[Navigation.NavigationStack.Count - 1]); // Manage Trip
            // And insert one with updated information.
            var itemView = Resolver.Resolve<TripDetailView>();
            var vm = itemView.BindingContext as TripViewModel;
            vm.Trip = Trip;
            await Navigation.PushAsync(itemView);
        });

        public ICommand TripFinish => new Command(async () =>
        {
            if (Trip.LocationFinishId == 0)
            {
                await App.Current.MainPage.DisplayAlert("Route incomplete",
                    "The route requires a finish location (gathered from 'stop' command) before marking as finished is allowed.", "OK");
                return;
            }

            if (this.trackingEnabled)
            {
                await App.Current.MainPage.DisplayAlert("Route in progress",
                    "The route is currently being tracked, please stop tracking before marking it as finished.", "OK");
                return;
            }

            // Completing a trip should update vehicle mileage.
            Vehicle.Mileage += Trip.MileageFinish - Trip.MileageStart;
            await vehicleRepository.AddOrUpdate(Vehicle);

            // Mark trip as finished and (un)lock controls.
            Trip.Finished = true;
            await tripRepository.AddOrUpdate(Trip);

            // HACK: Manipulate NavigationStack items till updatable stack solution is found.
            // Eventually update 'navigation backwards' page instead.
            // Remove previous two navigation stacks.
            Navigation.RemovePage(Navigation.NavigationStack[Navigation.NavigationStack.Count - 1]); // Route View
            Navigation.RemovePage(Navigation.NavigationStack[Navigation.NavigationStack.Count - 1]); // Manage Trip
            // And insert one with updated information.
            var itemView = Resolver.Resolve<TripDetailView>();
            var vm = itemView.BindingContext as TripViewModel;
            vm.Trip = Trip;
            await Navigation.PushAsync(itemView);
        });

        public ICommand TripMap => new Command(async () =>
        {
            var mapView = Resolver.Resolve<MapView>();
            var vm = mapView.BindingContext as MapViewModel;
            vm.Trip = Trip;
            //vm.Points = pointList;
            await Navigation.PushAsync(mapView);
        });

        public ObservableCollection<string> PositionList { get; set; } = new ObservableCollection<string>();
        private async void PositionChanged(object sender, PositionEventArgs e)
        {
            //If updating the UI, ensure you invoke on main thread.
            var position = e.Position;

            Debug.WriteLine("Desired Accuracy: {0}", CrossGeolocator.Current.DesiredAccuracy);
            // Since the CrossGeolocator doesn't skip points exceeding the Accuracy, do it here.
            if (position.Accuracy > CrossGeolocator.Current.DesiredAccuracy)
            {
                Debug.WriteLine("Skipped!");
                return;
            }
            PositionList.Add(String.Format("Sp:{0}, Acc:{1}, Alt:{2}, H:{3}, TS:{4}",
                position.Speed, position.Accuracy, position.AltitudeAccuracy, position.Heading, position.Timestamp.TimeOfDay));

            // Format a location.
            LocationNext = new Models.Location(Trip.Id, position.Latitude, position.Longitude, DateTime.Now, position.Speed);

            // Save location to the associated trip.
            await this.locationRepository.Save(LocationNext);

            // Append location to the list of registered positions.
            // This is done after the save action with PK's available as result.
            Positions.Add(LocationNext);

            // Ensure there is a previous location before calculating new averages.
            if (LocationPrev.Id == 0)
            {
                LocationPrev = LocationNext;
                return;
            }

            double distance = GeolocatorUtils.CalculateDistance(
                LocationPrev.Latitude, LocationPrev.Longitude,
                LocationNext.Latitude, LocationNext.Longitude,
                units: GeolocatorUtils.DistanceUnits.Kilometers);

            Debug.WriteLine("Diff in coordiantes: {0} km.", distance);

            double diffInMs = (LocationNext.Timestamp - LocationPrev.Timestamp).TotalMilliseconds;
            double speedAvg = distance / (diffInMs / 3600000); // km/h

            // Use smart calculations to avoid looping over all values.
            travelDistance += distance;
            // Use formulla to avoid iterating all times to calculate average.
            travelSpeedAvg += ((speedAvg - travelSpeedAvg) / Positions.Count);

            Debug.WriteLine("New calculated totals, duration: {0:hh\\:mm\\:ss}, traveled: {1:0.000} km, speed (avg): {2:0.0} km/h.",
                travelDuration, travelDistance, travelSpeedAvg);

            // Set the previous location to the current one.
            LocationPrev = LocationNext;
        }
    }
}
