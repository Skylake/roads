﻿using Plugin.Geolocator;
using Plugin.Geolocator.Abstractions;
using Roads.Controls;
using Roads.Models;
using Roads.Repositories;
using Roads.Services;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Essentials;
using Xamarin.Forms;
using Xamarin.Forms.Maps;

namespace Roads.ViewModels
{
    public class MapViewModel : ViewModel
    {
        private readonly ILocationRepository locationRepository;
        // TODO: Eventually add Vehicle for Vehicle Lookup?
        public CustomMap Map { get; set; }

        // Store the selected trip.
        private Trip trip;
        public Trip Trip
        {
            get { return trip; }
            set
            {
                if (trip != value && value.Id != 0)
                {
                    // Update trip value and notify the vm about the change.
                    trip = value;
                    RaisePropertyChanged(nameof(Trip));
                    Debug.WriteLine("App.TripId = " + Trip.Id);

                    // Call task to load existing trip information.
                    Task.Run(async () => await LoadData());
                }
            }
        }

        private ObservableCollection<Models.Point> points;
        public ObservableCollection<Models.Point> Points
        {
            get { return points; }
        }

        public MapViewModel(ILocationRepository locationRepository)
        {
            // Add event to OnItemAdded handler to bring awareness of new points.
            locationRepository.OnItemAdded += UpdateMapEvent;
            Debug.WriteLine("ATTACHED OnItemAdded -> UpdateMapEvent.");

            this.locationRepository = locationRepository;
            this.points = new ObservableCollection<Models.Point>();
            this.trip = new Trip();

            // TODO: Eventually disable back button to enforce usage of 'close'
            // to ensure proper clean-up is done.
        }

        private void UpdateMapEvent(object sender, Models.Location location)
        {
            // Avoid collection changed by waiting.
            if (!finishedLoading)
                return;

            if (location.TripId == this.Trip.Id)
            {
                Debug.WriteLine("UpdateMapEvent, trip: {0}, location: {1}.", Trip.Id, location.Id);

                this.Points.Add(new Models.Point() { Location = location });
                Debug.WriteLine("Points total count: {0}.", this.Points.Count);

                // Is TrackMe enabled?
                if (!this.lockedTracking)
                    return;

                // Move the map to the new added information.
                UpdateMapPosition(location.Latitude, location.Longitude);
            }
        }

        private bool finishedLoading { get; set; } = false;
        private async Task LoadData()
        {
            // Disable back button to enforce proper clean-up of event handler.
            // NavigationPage.SetHasBackButton(Navigation.NavigationStack[Navigation.NavigationStack.Count - 1], false);

            // Get a list of trip items to display.
            var locations = await this.locationRepository.GetTripRoute(Trip);
            Debug.WriteLine("Trip {0} loaded with {1} points available in database.", Trip.Id, locations.Count);

            Points.Clear();
            foreach (Models.Location location in locations)
            {
                Points.Add(new Models.Point() { Location = location });
            }

            Debug.WriteLine("Points available after LoadData(): {0}.", Points.Count);
            finishedLoading = true;

            // Move the map to the last loaded position (to fix position when not tracking).
            // Uses last location from repository - or previous known location if empty.
            if (locations.Count != 0) {
                UpdateMapPosition(locations.Last().Latitude, locations.Last().Longitude);
            } else {
                var lastPosition = await Geolocation.GetLastKnownLocationAsync();
                UpdateMapPosition(lastPosition.Latitude, lastPosition.Longitude);
            }
        }

        private void UpdateMapPosition(double latitude, double longitude)
        {
            MainThread.BeginInvokeOnMainThread(() =>
            {
                Map.MoveToRegion(MapSpan.FromCenterAndRadius(
                    new Xamarin.Forms.Maps.Position(latitude, longitude),
                    Distance.FromKilometers(5)));
            });
        }

        public bool lockedTracking { get; set; } = true;
        public string FollowText => lockedTracking ? "Locked" : "Unlocked";
        public ICommand FollowMe => new Command(() =>
        {
            this.lockedTracking = !this.lockedTracking;

            if (this.lockedTracking)
            {
                // Move the map to the last loaded position (also when not tracking).
                if (Points.Count != 0)
                {
                    UpdateMapPosition(
                        Points.Last().Location.Latitude,
                        Points.Last().Location.Longitude
                    );
                }
            }
        });


        // Display the route action and available points count.
        public ICommand RouteInfo => new Command(() =>
        {
            string routeAction = "Route Created!";
            string prevPoints = "";
            if (Trip.LocationStartId != 0)
            {
                routeAction = "Route Resumed!";
                prevPoints = "Your previous progress has been restored, ";
            }
            MainThread.BeginInvokeOnMainThread(async () =>
            {
                await App.Current.MainPage.DisplayAlert(routeAction,
                    prevPoints + "there are already " + Points.Count + " points registered.", "Ahoy, Matey!");
            });
        });

        // Stop 
        public ICommand CloseMap => new Command(async () =>
        {
            locationRepository.OnItemAdded -= UpdateMapEvent;
            Debug.WriteLine("DETACHED OnItemAdded -> UpdateMapEvent.");
            await Navigation.PopAsync();
        });

    }
}
