﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Windows.Input;
using Roads.Models;
using Roads.Repositories;
using Roads.Views;
using Xamarin.Forms;

namespace Roads.ViewModels
{
    public class VehicleViewModel : ViewModel
    {
        private VehicleRepository vehicleRepository;
        private TripRepository tripRepository;

        // Store the selected vehicle.
        private Vehicle vehicle;
        public Vehicle Vehicle
        {
            get { return vehicle; }
            set
            {
                if (vehicle != value)
                {
                    vehicle = value;
                    RaisePropertyChanged(nameof(Vehicle));

                    // HACK: Load form information when an existing vehicle is loaded.
                    // This resolves race condition where selected type
                    // can't be determinated.
                    if (vehicle.Id != 0)
                    {
                        SelectedType = vehicle.Type;

                        // After the vehicle has been set, load the bound trip.
                        Task.Run(async () => await LoadMostRecentTrip());
                    }
                }
            }
        }

        public Trip mostRecentTrip { get; set; }
        public bool vehicleDeletable { get; set; }
        public VehicleViewModel(VehicleRepository vehicleRepository,
            TripRepository tripRepository)
        {
            this.vehicleRepository = vehicleRepository;
            this.tripRepository = tripRepository;
            this.vehicleDeletable = false;
            this.mostRecentTrip = new Trip();
            this.Vehicle = new Vehicle()
            {
                PrivateUse = false,
                Available = true,
                DateAdded = DateTime.Now
            };
        }

        private async Task LoadMostRecentTrip()
        {
            var vehicleTrips = await tripRepository.GetVehicleTrips(Vehicle.Id);
            if (vehicleTrips.Count == 0)
            {
                vehicleDeletable = true;
                return;
            }

            int lastPositionId = 0;
            foreach (Trip prevTrip in vehicleTrips)
            {
                Location lastTripPosition = await tripRepository.GetTripLastPosition(prevTrip.Id);
                if (lastTripPosition.Id > lastPositionId)
                {
                    lastPositionId = lastTripPosition.Id;
                    mostRecentTrip = prevTrip;
                }
            }

            RaisePropertyChanged(nameof(mostRecentTrip));
        }

        private string selectedType;
        public string SelectedType
        {
            get { return selectedType; }
            set
            {
                if (selectedType != value)
                {
                    selectedType = value;
                    Vehicle.Type = value;
                    RaisePropertyChanged(nameof(SelectedType), nameof(Vehicle));
                }
            }
        }

        public ICommand Save => new Command(async () =>
        {
            if (String.IsNullOrWhiteSpace(Vehicle.Name) ||
                String.IsNullOrWhiteSpace(Vehicle.Manufacturer) ||
                String.IsNullOrWhiteSpace(Vehicle.Type) ||
                String.IsNullOrWhiteSpace(Vehicle.LicensePlate))
            {
                await App.Current.MainPage.DisplayAlert("Vehicle incomplete",
                    "One or more required fields are missing.", "OK");
                return;
            }

            await vehicleRepository.AddOrUpdate(Vehicle);
            await Navigation.PopAsync();
        });

        public ICommand Delete => new Command(async () =>
        {
            // Only try to remove if PK is assigned and no associated data is present.
            if (Vehicle.Id != 0 && mostRecentTrip.Id == 0)
            {
                await vehicleRepository.DeleteItem(Vehicle);
            }

            await Navigation.PopAsync();
        });


        public ICommand VehicleOnMap => new Command(async () =>
        {
            if (mostRecentTrip.Id == 0)
                return;

            var mapView = Resolver.Resolve<MapView>();
            var vm = mapView.BindingContext as MapViewModel;
            vm.Trip = mostRecentTrip;
            // TODO: Eventually create a 'MapMode' to determinate conditions.
            vm.lockedTracking = false;
            await Navigation.PushAsync(mapView);
        });
    }
}