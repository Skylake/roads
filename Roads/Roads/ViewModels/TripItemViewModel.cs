﻿using System;
using System.Diagnostics;
using System.Windows.Input;
using Plugin.Geolocator;
using Roads.Models;
using Roads.Repositories;
using Roads.Views;
using Xamarin.Forms;

namespace Roads.ViewModels
{
    public class TripItemViewModel : ViewModel
    {
        public TripItemViewModel(Trip trip) {
            Trip = trip;
        }

        public event EventHandler ItemStatusChanged;
        public Trip Trip { get; private set; }
        public Vehicle Vehicle { get; private set; } // needed?
        public string StatusText => Trip.Finished ? "Finished" : "Unfinished";
        public string ProgressText => String.Format(
                (String.IsNullOrEmpty(Trip.LocationStartAddress)) ? "{2}" : "{0} > {1}, {2}",
            Trip.LocationStartAddress, Trip.LocationFinishAddress,
            Trip.DateTripStart is null ? "Not started yet" :
            Trip.DateTripStart.Value.ToString("MMMM d, yyyy"));

        public ICommand ToggleFinished => new Command(async () =>
        {
            if (!Trip.Finished && Trip.LocationFinishId == 0)
            {
                await App.Current.MainPage.DisplayAlert("Trip incomplete",
                    "This trip is incomplete and cannot be finished yet.", "OK");
                return;
            }

            // Toggle finished state.
            Trip.Finished = !Trip.Finished;
            ItemStatusChanged?.Invoke(this, new EventArgs());
        });
    }
}
