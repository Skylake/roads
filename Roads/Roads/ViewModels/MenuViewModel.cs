﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Windows.Input;
using Plugin.BLE;
using Roads.Repositories;
using Roads.Services;
using Roads.Views;
using Xamarin.Essentials;
using Xamarin.Forms;

namespace Roads.ViewModels
{
    public class MenuViewModel : ViewModel
    {
        private readonly ILocationTrackingService locationTrackingService;


        public MenuViewModel(ILocationTrackingService locationTrackingService)
        {
            this.locationTrackingService = locationTrackingService;

            MainThread.BeginInvokeOnMainThread(async () =>
            {
                await locationTrackingService.StartListening();
            });
        }

        public ICommand Vehicles => new Command(async () =>
        {
            var selectVehicleView = Resolver.Resolve<VehicleSelectView>();
            await Navigation.PushAsync(selectVehicleView);
        });

        public ICommand ManageVehicles => new Command(async () =>
        {
            //var selectVehicleView = Resolver.Resolve<VehicleSelectView>();
            //await Navigation.PushAsync(selectVehicleView);

            Models.Location location = await locationTrackingService.GetLocation();
            Debug.WriteLine($"{location.Latitude}, {location.Longitude}");
        });

        public ICommand ManageTrips => new Command(async () =>
        {
            var selectTripView = Resolver.Resolve<TripSelectView>();
            await Navigation.PushAsync(selectTripView);
        });

        public ICommand DisplayMap => new Command(async () =>
        {
            var mapView = Resolver.Resolve<MapView>();
            await Navigation.PushAsync(mapView);
        });

        public ICommand ManageBluetooth => new Command(async () =>
        {
            var ble = CrossBluetoothLE.Current;
            var adapter = CrossBluetoothLE.Current.Adapter;

            // Get the bluetooth state.
            var state = ble.State;

            // Listen for bluetooth state changes.
            ble.StateChanged += (s, e) =>
            {
                Debug.WriteLine($"The bluetooth state changed to {e.NewState}");
            };

            // Scan for devices.
            // deviceList.Clear();

            adapter.ScanMode = Plugin.BLE.Abstractions.Contracts.ScanMode.Balanced;
            adapter.ScanTimeout = 10000;
            adapter.DeviceDiscovered += (s, a) =>
            {
                Debug.WriteLine($"Detected a new device: {a.Device.Id}, {a.Device.Name}, {a.Device.Rssi}.");
                //foreach (var advertisementKey in a.Device.AdvertisementRecords)
                //{
                //    Debug.WriteLine($"{advertisementKey}");
                //}
            };
            await adapter.StartScanningForDevicesAsync();

            //var systemDevices = adapter.GetSystemConnectedOrPairedDevices();
            //foreach (var device in systemDevices)
            //{
            //    Console.WriteLine($"Paired or connected with: {device.Id}, {device.Name}.");
            //}

            Debug.WriteLine($"Is scanning: {adapter.IsScanning}");
        });
    }
}