﻿using Roads.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Essentials;
using Xamarin.Forms;
using Xamarin.Forms.Maps;
using Xamarin.Forms.Xaml;

namespace Roads.Views
{
    public partial class MapView : ContentPage
    {
        public MapView(MapViewModel viewModel)
        {
            InitializeComponent();
            viewModel.Navigation = Navigation;
            viewModel.Map = Map;
            BindingContext = viewModel;
            
        }
    }
}