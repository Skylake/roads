﻿using System;
using System.Collections.Generic;
using Roads.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Roads.Views
{
    public partial class TripDetailView : ContentPage
    {
        public TripDetailView(TripViewModel viewModel)
        {
            InitializeComponent();
            viewModel.Navigation = Navigation;
            BindingContext = viewModel;
        }
    }
}
