﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Plugin.Geolocator;
using Roads.ViewModels;
using Xamarin.Forms;

namespace Roads.Views
{
    public partial class RouteView : ContentPage
    {
        public RouteView(RouteViewModel viewModel)
        {
            InitializeComponent();
            viewModel.Navigation = Navigation;
            BindingContext = viewModel;


            //BindingContext = viewModel;
            //btnGetLocation.Clicked += BtnGetLocation_Clicked;
        }

        /* private async void BtnGetLocation_Clicked(object sender, EventArgs e)
        {
            await RetreiveLocation();
        }

        private async Task RetreiveLocation()
        {
            var locator = CrossGeolocator.Current;
            locator.DesiredAccuracy = 20;

            var position = await locator.GetPositionAsync(timeout: new TimeSpan(0, 0, 10));

            txtLat.Text = "Latitude: " + position.Latitude.ToString();
            txtLong.Text = "Longitude: " + position.Longitude.ToString();

            //MyMap.MoveToRegion(MapSpan.FromCenterAndRadius(new Position(position.Latitude, position.Longitude)
            //                 , Distance.FromMiles(1)));
        } */
    }
}
