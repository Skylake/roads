﻿using System;
using System.Collections.Generic;
using Roads.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Roads.Views
{
    public partial class TripSelectView : ContentPage
    {
        public TripSelectView(TripSelectViewModel viewModel)
        {
            InitializeComponent();
            viewModel.Navigation = Navigation;
            BindingContext = viewModel;

            ItemsListView.ItemSelected += (s, e) => ItemsListView.SelectedItem = null;
        }
    }
}
