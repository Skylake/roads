﻿using System;
using System.Collections.Generic;
using Roads.ViewModels;
using Xamarin.Forms;

namespace Roads.Views
{
    public partial class MenuView : ContentPage
    {
        public MenuView(MenuViewModel viewModel)
        {
            InitializeComponent();
            viewModel.Navigation = Navigation;
            BindingContext = viewModel;
        }
    }
}
