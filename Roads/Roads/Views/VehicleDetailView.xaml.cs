﻿using System;
using System.Collections.Generic;
using Roads.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Roads.Views
{
    public partial class VehicleDetailView : ContentPage
    {
        public VehicleDetailView(VehicleViewModel viewModel)
        {
            InitializeComponent();
            viewModel.Navigation = Navigation;
            BindingContext = viewModel;
        }
    }
}
