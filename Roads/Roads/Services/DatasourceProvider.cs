﻿using System;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Roads.Models;
using SQLite;

namespace Roads.Services
{
    public class DatasourceProvider : DbContext, IDatasourceProvider
    {
        private SQLiteAsyncConnection _connection;

        public DatasourceProvider()
        {
            if (_connection != null)
            {
                return;
            }

            var libFolder = Xamarin.Essentials.FileSystem.AppDataDirectory;
            var databasePath = Path.Combine(libFolder, "Roads.db");

            _connection = new SQLiteAsyncConnection(databasePath);
            _connection.CreateTableAsync<Location>().Wait();
            _connection.CreateTableAsync<Vehicle>().Wait();
            _connection.CreateTableAsync<Trip>().Wait();
        }

        public async Task InitializeDatabase()
        {
            // TODO: Create initial records.
            if (await _connection.Table<Vehicle>().CountAsync() == 0)
            {
                await _connection.InsertAsync(new Vehicle()
                {
                    Name = "Create your first vehicle.",
                    PrivateUse = true,
                    Available = true,
                    DateAdded = DateTime.Now,
                });
            }

            if (await _connection.Table<Trip>().CountAsync() == 0)
            {
                /* await _connection.InsertAsync(new Trip()
                {
                    Name = "Create your first trip.",
                    DateTripStart = DateTime.Now,
                    Finished = false
                }); */
            }
        }

        public SQLiteAsyncConnection GetDatasourceConnection()
        {
            return _connection;
        }

        // Mocking elements.
        public DbSet<Location> Locations { get; set; }
        public DbSet<Trip> Trips { get; set; }
        public DbSet<Vehicle> Vehicles { get; set; }

        IQueryable<T> IDatasourceProvider.Set<T>()
        {
            return base.Set<T>();
        }
    }
}
