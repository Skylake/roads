﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Roads.Services
{
    public interface IDatasourceProvider
    {
        IQueryable<T> Set<T>() where T : class;
    }
}
