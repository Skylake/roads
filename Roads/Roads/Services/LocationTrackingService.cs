﻿using System;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Plugin.Geolocator;
using Plugin.Geolocator.Abstractions;
using Roads.Repositories;
using Xamarin.Essentials;

namespace Roads.Services
{
    public class LocationTrackingService : ILocationTrackingService
    {
        public Models.Location Location { get; set; }
        public ObservableCollection<Models.Location> Positions;
        // todo: get location and get positions in interface?

        public LocationTrackingService()
        {
            Location    = new Models.Location();
            Positions   = new ObservableCollection<Models.Location>();         
        }

        public async Task<Position> GetCurrentPosition()
        {
            Position position = null;
            try
            {
                var locator = CrossGeolocator.Current;
                locator.DesiredAccuracy = 50;

                position = await locator.GetLastKnownLocationAsync();
                
                if (position != null)
                {
                    //got a cahched position, so let's use it.
                    return position;
                }

                if (!locator.IsGeolocationAvailable || !locator.IsGeolocationEnabled)
                {
                    //not available or enabled
                    return null;
                }

                position = await locator.GetPositionAsync(TimeSpan.FromSeconds(20), null, true);

            }
            catch (Exception ex)
            {
                Debug.WriteLine("Unable to get location: " + ex);
            }

            if (position == null)
                return null;

            var output = string.Format("Time: {0} \nLat: {1} \nLong: {2} \nAltitude: {3} \nAltitude Accuracy: {4} \nAccuracy: {5} \nHeading: {6} \nSpeed: {7}",
                    position.Timestamp, position.Latitude, position.Longitude,
                    position.Altitude, position.AltitudeAccuracy, position.Accuracy, position.Heading, position.Speed);

            Debug.WriteLine(output);

            return position;
        }

        public async Task<string> GetPositionAddress(Models.Location location)
        {
            //var locator = CrossGeolocator.Current;

            //try
            //{
            //    string mapKey = null; //only needed on UWP
            //    var addresses = await locator.GetAddressesForPositionAsync(position, mapKey);
            //    var address = addresses.FirstOrDefault();

            //    if (address == null)
            //        Console.WriteLine("No address found for position.");
            //    else
            //        Console.WriteLine("Addresss: {0} {1} {2}", address.Thoroughfare, address.Locality, address.CountryCode);
            //}
            //catch (Exception ex)
            //{
            //    Debug.WriteLine("Unable to get address: " + ex);
            //}

            Placemark placemark = null;
            string positionAddress = "";


            // Try to receive and set starting location.
            try
            {
                var placemarks = await Geocoding.GetPlacemarksAsync(location.Latitude, location.Longitude);
                placemark = placemarks?.FirstOrDefault();

                if (placemark != null)
                {
                    var geocodeAddress =
                        $"AdminArea:       {placemark.AdminArea}\n" +
                        $"CountryCode:     {placemark.CountryCode}\n" +
                        $"CountryName:     {placemark.CountryName}\n" +
                        $"FeatureName:     {placemark.FeatureName}\n" +
                        $"Locality:        {placemark.Locality}\n" +
                        $"PostalCode:      {placemark.PostalCode}\n" +
                        $"SubAdminArea:    {placemark.SubAdminArea}\n" +
                        $"SubLocality:     {placemark.SubLocality}\n" +
                        $"SubThoroughfare: {placemark.SubThoroughfare}\n" +
                        $"Thoroughfare:    {placemark.Thoroughfare}\n";

                    positionAddress = placemark.FeatureName;
                    Debug.WriteLine(geocodeAddress);
                }
            }
            catch (FeatureNotSupportedException fnsEx)
            {
                // Feature not supported on device
                Debug.WriteLine(fnsEx.Message);
            }
            catch (Exception ex)
            {
                // Handle exception that may have occurred in geocoding
                Debug.WriteLine(ex.Message);
            }

            return positionAddress;
        }

        public async Task<Models.Location> GetLocation()
        {
            Position position = await GetCurrentPosition();
            if (position == null)
                return new Models.Location();

            return new Models.Location(TripId: 0, position.Latitude, position.Longitude, DateTime.Now, position.Speed);
        }

        public ObservableCollection<Models.Location> GetPositions()
        {
            return this.Positions;
        }

        public async Task StartListening()
        {
            if (CrossGeolocator.Current.IsListening)
                return;

            await CrossGeolocator.Current.StartListeningAsync(TimeSpan.FromSeconds(5), 10, true); // 5 / 10

            CrossGeolocator.Current.PositionChanged += PositionChanged;
            CrossGeolocator.Current.PositionError += PositionError;
        }

        public async Task StartServiceListening()
        {
            if (CrossGeolocator.Current.IsListening)
                return;

            // Set desired accuracy.
            CrossGeolocator.Current.DesiredAccuracy = 50;

            // This logic will run on the background automatically on iOS, however for Android and UWP it is still required to
            // put logic in background services. Else if your app is killed the location updates will be killed.
            await CrossGeolocator.Current.StartListeningAsync(TimeSpan.FromSeconds(3), 10, false, new ListenerSettings // FromSeconds(5) / 10, true
            {
                ActivityType = ActivityType.Fitness, // AutomotiveNavigation,
                AllowBackgroundUpdates = true,
                DeferLocationUpdates = false, // true
                DeferralDistanceMeters = 10, // 10, // 1
                DeferralTime = TimeSpan.FromSeconds(3), // FromSeconds(1)
                ListenForSignificantChanges = false, // true
                PauseLocationUpdatesAutomatically = false
            });

            CrossGeolocator.Current.PositionChanged += PositionChanged;
            CrossGeolocator.Current.PositionError += PositionError;
        }

        public async Task StopListening()
        {
            if (!CrossGeolocator.Current.IsListening)
                return;

            await CrossGeolocator.Current.StopListeningAsync();

            CrossGeolocator.Current.PositionChanged -= PositionChanged;
            CrossGeolocator.Current.PositionError -= PositionError;
        }

        public void PositionChanged(object sender, PositionEventArgs e)
        {
            //If updating the UI, ensure you invoke on main thread
            var position = e.Position;
            var output = "Full: Lat: " + position.Latitude + " Long: " + position.Longitude;
            output += "\n" + $"Time: {position.Timestamp}";
            output += "\n" + $"Heading: {position.Heading}";
            output += "\n" + $"Speed: {position.Speed} m/s ({position.Speed * 3.6} km/h)";
            output += "\n" + $"Accuracy: {position.Accuracy}";
            output += "\n" + $"Altitude: {position.Altitude}";
            output += "\n" + $"Altitude Accuracy: {position.AltitudeAccuracy}";
            // Debug.WriteLine(output);

            // Format a location.
            this.Location = new Models.Location(0, position.Latitude, position.Longitude, DateTime.Now, position.Speed);

            // Append location to the list of registered positions.
            Positions.Add(this.Location);
        }

        public void PositionError(object sender, PositionErrorEventArgs e)
        {
            Debug.WriteLine(e.Error);
            //Handle event here for errors
        }

        public double GetTotalDistance()
        {
            // Print all travel positions.
            double totalDistance = 0;
            Models.Location prevPosition = new Models.Location();
            foreach (Models.Location nextPosition in Positions)
            {
                if (prevPosition.Latitude == 0 || prevPosition.Latitude == 0)
                {
                    prevPosition = nextPosition;
                    continue;
                }

                var distance = GeolocatorUtils.CalculateDistance(
                    prevPosition.Latitude, prevPosition.Longitude,
                    nextPosition.Latitude, nextPosition.Longitude,
                    units: GeolocatorUtils.DistanceUnits.Kilometers);

                totalDistance += distance;

                // Update prevPosition to compare between following points.
                prevPosition = nextPosition;
            }

            Debug.WriteLine("Distance (total): {0} km.", totalDistance);
            return totalDistance;
        }
    }
}
