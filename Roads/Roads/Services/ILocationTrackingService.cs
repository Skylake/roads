﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using System.Threading.Tasks;
using Plugin.Geolocator.Abstractions;
using Roads.Models;

namespace Roads.Services
{
    public interface ILocationTrackingService
    {
        //event EventHandler<PositionEventArgs> PositionChanged;
        //event EventHandler<PositionErrorEventArgs> PositionError;

        Task<Position> GetCurrentPosition();
        Task<Location> GetLocation();
        Task<string> GetPositionAddress(Location location);

        ObservableCollection<Location> GetPositions();

        Task StartListening();
        Task StartServiceListening();
        Task StopListening();

        double GetTotalDistance();

        // Task StartListening(Action<object, PositionEventArgs> positionChanged);
        //void PositionChanged(object sender, PositionEventArgs e);
        //void PositionError(object sender, PositionErrorEventArgs e);

    }
}
