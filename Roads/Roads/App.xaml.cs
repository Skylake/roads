﻿using Roads.Services;
using Roads.Views;
using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Roads
{
    public partial class App : Application
    {
        private bool isStartUp;

        public App()
        {
            InitializeComponent();

            // Set the default navigation page at launch.
            MainPage = new NavigationPage(Resolver.Resolve<TripSelectView>());
            isStartUp = false;
        }

        protected override void OnStart()
        {
        }

        protected override void OnSleep()
        {
        }

        protected override void OnResume()
        {
            if (!isStartUp)
            {
                // Eventually resume certain page after navigating back to the app.
                // This is not set to resume the last activity.
                // MainPage = new NavigationPage(Resolver.Resolve<TripSelectView>());
            }
        }
    }
}
