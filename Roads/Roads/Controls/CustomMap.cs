﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Diagnostics;
using Xamarin.Forms;
using Xamarin.Forms.Maps;

namespace Roads.Controls
{
    public class CustomMap : Map
    {
        public static BindableProperty PointsProperty =
            BindableProperty.Create(nameof(Points),
                typeof(ObservableCollection<Models.Point>), typeof(CustomMap),
                new ObservableCollection<Models.Point>());

        public ObservableCollection<Models.Point> Points
        {
            get => GetValue(PointsProperty) as ObservableCollection<Models.Point>;
            set => SetValue(PointsProperty, value);
        }
    }
}
